# 600 Watt Hauptquartier

## Zweck dieses Repositories

Die Bereitstellung von übergreifenden und öffentlichen

- [Issues](https://codeberg.org/600watt/Hauptquartier/issues)
- [Kanban-Boards](https://codeberg.org/600watt/Hauptquartier/projects)

## Wozu dienen Issues?

Issues dienen der offenen und strukturierten Kommunikation. Wie verwenden Issues für:

- **Fragen an 600 Watt**, und Antworten auf diese Fragen. Hast Du eine Frage an uns? Dann schau bitte nach, ob diese schon als Issue gestellt wurde. Es lohnt sich auch ein blick in bereits geschlossene Issues, denn beantwortete Fragen werden geschlossen. Hast Du eine neue Frage, dann erstelle bitte ein Issue.
- **Aufgaben**, also Dinge, die wir möglicherweise mal erledigen wollen. Wir bemühen uns, Aufgaben nach Priorität zu kennzeichnen. Erledigte Aufgaben werden geschlossen.

Für die Mitarbeit an Issues, sowohl das Erstellen als auch das Kommentieren, benötigst Du ein Codeberg Benutzerkonto. Keine Angst, Codeberg ist vertrauenswürdig, sonst wären wir nicht hier.

## Wozu dienen Kanban-Boards?

Über Kanban-Boards kann man einen Überblick über Issues bekommen und sie gruppieren und sortieren, beispielsweise um die Reihenfolge der Bearbeitung festzulegen.
